import logo from './logo.svg';
import './App.css';
import Navigation from './Navigation/Navigation.js'
import Home from './Pages/Home.js'
import Calendar from './Pages/Calendar.js'
import Members from './Pages/Members.js'
import PullRequest from './Pages/PullRequest.js'
import Slack from './Pages/Slack.js'
import Wiki from './Pages/Wiki.js'
import Retro from './Pages/Retro.js'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

function App() {
  return (
    <>
    <Navigation />
    
    <Router>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/members" component={Members}/>
        <Route path="/calendar" component={Calendar}/>
        <Route path="/Slack" component={Slack}/>
        <Route path="/wiki" component={Wiki}/>
        <Route path="/pr" component={PullRequest}/>
        <Route path="/retro" component={Retro}/>
      </Switch>
    </Router>
      
    
    </>  
  );
}

export default App;
