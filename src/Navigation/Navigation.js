import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { MenuItems } from './MenuItems.js'

class Navigation extends React.Component {

    state = {
        loading: true,
        team: null
    }

    async componentDidMount(){
        const url = "https://localhost:44331/teams/1"
        const response = await fetch(url);
        const data = await response.json();
        console.log(data);
    
        this.setState({team: data, loading: false})
    }

    render() {
        return(
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">{(this.state.loading) ? "Henter..." : this.state.team.name}</Navbar.Brand>
          <Nav className="mr-auto">

            {MenuItems.map((item, i) => {
              return(
                <Nav.Link href={item.url}>{item.title}</Nav.Link>
              )
            }
            )}
            
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-info">Search</Button>
          </Form>
        </Navbar>
        )
    }
  }

  export default Navigation;