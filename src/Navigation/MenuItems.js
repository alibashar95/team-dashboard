export const MenuItems = [
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'Members',
        url: '/members'
    },
    {
        title: 'Calendar',
        url: '/calendar'
    },
    {
        title: 'Slack',
        url: '/slack'
    },
    {
        title: 'Wiki',
        url: '/wiki'
    },
    {
        title: 'Pull Request',
        url: '/pr'
    },
    {
        title: 'Retrospective',
        url: '/retro'
    },
    
]